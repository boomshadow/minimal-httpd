This repo is for the Docker Hub image located at <https://hub.docker.com/r/boomshadow/minimal-httpd/>

It is an extremely fast, lightweight HTTP server for testing purposes. It only responds with "200 OK".  
Its powered by BusyBox `httpd` on Alpine, with a basic `tini` init system.

Special thanks to: [Big list of http static server one-liners](https://gist.github.com/willurd/5720255)
