FROM alpine:latest

RUN apk add --no-cache tini busybox-extras
COPY index.html /

EXPOSE 80

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["busybox-extras", "httpd", "-f", "-p", "80"]
